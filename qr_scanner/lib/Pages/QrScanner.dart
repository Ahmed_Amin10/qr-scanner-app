// ignore_for_file: prefer_const_constructors

import 'dart:async';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gsheets/gsheets.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';

const _credintial = r'''
{
  "type": "service_account",
  "project_id": "qr-app-ieee-nu",
  "private_key_id": "cc2ac7eee82ed934fabbf018951aaee9eaf3143c",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDRHzgHx/nhntnJ\nAMSMOr6J8dKd0Ydt9T2VqXsCRjmSvMAIr0NsNlzLojODDSBm0OjZrWDyDpfDSD0N\nbGTvejpx6l8lCTKsCAxuGCC+1IK8OaZlITA/MCRJTyLEgMMhi9A5w34NcUta34jc\nGmKjwist+FwPNpUQjIBgxOmPNPPyUTmbEHFDkDz17DfFc1gZY10C4miU1LETkJSL\nJ+dQwAJLK53U6YWctmx5Xrbv2wgLWIh9AKLByZ52xtGo59EbEzXOMWDahVH5pK17\nvciQNenIZIZ/IzkL5lGZ317cbrnD5nPmzfWkxjifU25o80JRAI3s4xLT8vpj5nc2\naOIbvPyvAgMBAAECggEAHr1RjadCHn2sssS3n4tVDc7rsEIn/6xQcKr4h3WOKfkv\n2zb0GLiG/HHr04Q+8o4b5D99pCcr/MsmGk6IKcGUTMFwWe/XsH0AYsCOM67mRsfM\no0n1HxyzAMmuqSbqSAiWw57ubQht1L5ODlicjkjQ8xFyatQRplyY1GCtmlzkzQYK\nLE4HW0MhIkqcheYJ6CwA6k0HgdyPLp7uWIoS3BtzOO5A9rDGl2FU2hhqH/2pSXA1\na/YQvbNBue+aP+obYGSDNONrgFdpxuPZkhlUoW5jO3N3wrLnw0rkyg+Sad2UPbxf\n7maQ9pvVxjZHYEOJgXgVIz2/4/6QNMC/vhc54uCbfQKBgQD//t5oR2ZsWp5VJjh2\n33dUwKeSaCJ5kVYHjDeQVu1IYNk5AfQzFmATuMTPC0y5r+xrDL7+4ZobAfXBMT+G\nXX9f86uSqEj9GvZvc0Uhoj4ezoIVBuQkBoxNeygykMtmj405kaIOLxh33ejJScDT\n0/Q3A6HL94mixrVL347dV5aItQKBgQDRICSZAgzCT94bXtAp/k5GFpIBFMO6Vzc+\nQhEU3k+WoNh2T6nU5HAJmpSBZk8NgDNlitgOeFJJliGysALf8J2cp+VeRe3h0r7d\nh/0rkP+gulPhuhQ0whokujdyMynhGdaE/4YWHm22fyqWEQx2rE6+s0lMcsSeTjqT\nmzop6fBCUwKBgGa2JRhj5XV+Gl8BudAAM0nfDp0QiRyLsp/Oo5CXQBc1PJLB9O/2\nf1AKZ3THDN7H9iyxbYSokgNaEveTwfAwXWXrR6DNQpPDGLN45XjsNs53fvveyvSl\nZ+/ChUtciy+4UyqRAxt7y+50K+1vWR3kwey82VMUG7Yi0iUCKrkmZnCtAoGBAJaY\nCQLOQRXkodMf8dNruD4snrN8mn4jtDp3JQncFYApD/gS4f/XCP9Be9O7Mw5L1bc3\nxuKUrt0i6d1ddpSBF17qYPgEb4uBUcUB2sR2xD6gLyuL7mUXqe/s0WTBh1T8YEii\nIZLoEqWq6aXrXQhQiNw3C3+r3f8J4pDM5ZOtTUDVAoGALkw6A+6xi0C4K/MYQKuk\nkOXah7h7yKslWgv/NboHPM/3ZhVjKnKbTC1PEnxPlLlRHvOL+NbdVFDQgCjYDQgr\nsphotaE4IhUkSY8AhUu07nrxf29LsvcUaZ8QSe3C5p+h8k7TJqRSVeyxtBt7SuCw\n3IBc+MFpQlI2pBbtNrc6lCk=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-olmg2@qr-app-ieee-nu.iam.gserviceaccount.com",
  "client_id": "106295995533119147766",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-olmg2%40qr-app-ieee-nu.iam.gserviceaccount.com",
  "universe_domain": "googleapis.com"
}
''';

const _spreadsheetId = '1VFxbsBh2QIX6GudNI7tVpRs0VRv_8gpU2xFIQIv7SUw';

Timer? timer;
bool refreshingData = false;

class QrScanner extends StatefulWidget {
  const QrScanner({Key? key}) : super(key: key);

  @override
  State<QrScanner> createState() => _QrScannerState();
}

class _QrScannerState extends State<QrScanner> {
  late QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  String scannedData = '';
  int scannedDataCount = 0;
  Color containerColor = Colors.white; // Initialize to white color
  bool processingScan = false; // Flag to indicate if a scan is being processed
  bool refreshingData = false; // Flag to indicate if data refresh is in progress

  @override
  void initState() {
    super.initState();
    _requestPermission();

    // Start a timer to refresh data every 60 seconds
    Timer.periodic(Duration(seconds: 60), (timer) {
      if (!processingScan && !refreshingData) {
        refreshingData = true;
        ReadData();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      if (!processingScan && !refreshingData) {
        setState(() {
          final qrData = scanData.code!;
          print('Scanned data: $qrData');
          if (qrData != scannedData) {
            _handleScannedData(qrData);
          }
        });
      }
    });
  }

  void _handleScannedData(String qrData) async {
    setState(() {
      processingScan = true; // Set flag to indicate processing of scan
    });

    if (qrData.isNotEmpty && qrData != scannedData) {
      final docRef =
          FirebaseFirestore.instance.collection('QrData').doc(qrData);

      FirebaseFirestore.instance.runTransaction((transaction) async {
        final docSnapshot = await transaction.get(docRef);

        if (docSnapshot.exists) {
          final currentCount = docSnapshot.data()?['count'] ?? 0;
          final newCount = currentCount + 1;

          transaction.update(docRef, {'count': newCount});

          setState(() {
            scannedDataCount = newCount;
            scannedData = qrData;
            containerColor = _getColorForCount(newCount); // Update color
          });

          print('Count incremented for QR data: $qrData');
        } else {
          print('Document does not exist for QR data: $qrData');
        }
      }).catchError((error) {
        print("Failed to increment count: $error");
      });

      // Wait for a short duration before allowing another scan
      await Future.delayed(Duration(seconds: 1));

      // Reset flag after processing the scan
      setState(() {
        processingScan = false;
      });
    }
  }

Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.grey,
      title: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0), // Add padding below the text
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "IEEE NU",
                style: GoogleFonts.libreBaskerville(
                  textStyle: const TextStyle(
                    fontSize: 32.0,
                    color: Color(0xFF001B94),
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ),
    backgroundColor: Colors.grey,
    body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          child: SizedBox(
            width: 350,
            height: 350,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
        ),
        const SizedBox(height: 40.0),
        Container(
          width: 300,
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: containerColor, // Use the containerColor variable here
          ),
          child: Text(
            scannedData.isEmpty ? 'Scanning...' : scannedData,
            style: TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10),
       
        SizedBox(
          width: 250,
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
           
           
           
            child: MaterialButton(
              onPressed: () {
                setState(() {
                  scannedData = '';
                  scannedDataCount = 0;
                  containerColor = Colors.white; // Reset color to white
                });
                controller.resumeCamera(); // Refresh camera
              },
              color: Color(0xFF001B94),
              textColor: Colors.white,
              padding: const EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                'Reset',
                style: const TextStyle(fontSize: 16),
              ),
            ),
          ),
        ),
        const SizedBox(height: 8), // Add some space before the creator text
        Text(
          'Created by Operations Head Ahmed Amin',
          style: GoogleFonts.libreBaskerville(fontSize: 14,
          color: Color(0xFF001B94),
          fontWeight: FontWeight.bold),
        ),
      ],
    ),
  );
}


  Color _getColorForCount(int count) {
    if (count >= 0 && count < 2) {
      return Colors.green;
    } else if (count >= 2) {
      return Colors.red;
    } else {
      return Colors.white; // Default color if count is negative
    }
  }

  void _requestPermission() async {
    // Assuming the permission is already granted
    print('Storage permission granted');
    ReadData();
  }

  void ReadData() async {
    try {
      final gsheets = GSheets(_credintial);
      final ss = await gsheets.spreadsheet(_spreadsheetId);
      final sheet = ss.worksheetByTitle('teest');
      final rows = await sheet?.values.map.allRows();

      if (rows != null) {
        final firestore = FirebaseFirestore.instance;
        final storage = FirebaseStorage.instance;

        for (var row in rows) {
          if (row.isNotEmpty) {
            String? name = row['name']; // Change to 'name' column

            if (name != null && name.isNotEmpty) {
              print('Checking for name: $name');

              final qrData =
                  await firestore.collection('QrData').doc(name).get();

              if (!qrData.exists) {
                final qrImage = QrPainter(
                  data: name, // Change to 'name'
                  version: QrVersions.auto,
                  color: Colors.white,
                  gapless: true,
                  errorCorrectionLevel: QrErrorCorrectLevel.L,
                );

                final imageData =
                    await qrImage.toImageData(200, format: ImageByteFormat.png);

                final storageRef = storage
                    .ref()
                    .child('qr_images/$name.png'); // Change to 'name'
                await storageRef.putData(imageData!.buffer.asUint8List());

                final String downloadURL = await storageRef.getDownloadURL();

                await firestore.collection('QrData').doc(name).set({
                  'count': 0,
                  'qr_image_url': downloadURL,
                });

                print('Data stored successfully in Firestore for name: $name');
              } else {
                print('QR code already exists for name: $name. Skipping.');
              }
            } else {
              print('Name is null or empty');
            }
          }
        }
      } else {
        print('No data fetched from Google Sheets.');
      }

      // Set refreshingData flag to false after completing data refresh
      refreshingData = false;
    } catch (e) {
      print('Error fetching data: $e');
      // Set refreshingData flag to false in case of error
      refreshingData = false;
    }
  }
}
